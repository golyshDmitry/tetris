from tkinter import *
from tkinter import messagebox
import pymongo

conn = pymongo.MongoClient('localhost', 27017)
db = conn.tetris_db
coll = db.users

'''for i in coll.find():
    print(i)'''

user = ""

root = Tk()
entered = False

root.geometry("300x500")
root.title("Войти в систему")


def registration():
    text1 = Label()
    text6 = Label()
    text = Label(text="Регистрация")
    text_log = Label(text="Введите логин:")
    reg_login = Entry()
    text_pass1 = Label(text="Введите пароль:")
    reg_pass1 = Entry(show="*")
    text_pass2 = Label(text="Повторите пароль:")
    reg_pass2 = Entry(show="*")
    button_reg = Button(text="Зарегистрироваться", command=lambda: save())
    text2 = Label()
    text3 = Label()
    text4 = Label()
    text5 = Label()

    text1.pack()
    text6.pack()
    text.pack()
    text_log.pack()
    reg_login.pack()
    text_pass1.pack()
    reg_pass1.pack()
    text_pass2.pack()
    reg_pass2.pack()
    button_reg.pack()
    text2.pack()
    text3.pack()
    text4.pack()
    text5.pack()

    def save():
        global coll
        if coll.find({"login" : reg_login.get()}).count() != 0:
            messagebox.showerror("Данное имя пользователя уже занято")
        else:
            if reg_pass1.get() != reg_pass2.get():
                messagebox.showerror("Пароли не совпадают!")
            else:

                coll.save({"login": reg_login.get(), "password": reg_pass1.get(), "score": 0})
                reg_login.delete(0, 'end')
                reg_pass1.delete(0, 'end')
                reg_pass2.delete(0, 'end')


def login():
    text = Label(text="Вход")
    text_log = Label(text="Введите логин:")
    enter_login = Entry()
    text_pass = Label(text="Введите пароль:")
    enter_pass = Entry(show="*")
    button_enter = Button(text="Войти", command=lambda: log_pass())

    text.pack()
    text_log.pack()
    enter_login.pack()
    text_pass.pack()
    enter_pass.pack()
    button_enter.pack()

    def log_pass():
        password = enter_pass.get()
        login = enter_login.get()
        global coll

        if coll.find({"login" : login, "password" : password}).count() != 0:
            messagebox.showinfo("Вход выполнен!")
            global entered
            entered = True
            root.destroy()
            global user
            user = login
        else:
            messagebox.showerror("Неверный логин или пароль!")


registration()
login()

root.mainloop()
